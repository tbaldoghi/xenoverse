// I have no idea why I wrote this... ¯\_(ツ)_/¯

class Input {
  static keyW: Phaser.Input.Keyboard.Key;
  static keyA: Phaser.Input.Keyboard.Key;
  static keyS: Phaser.Input.Keyboard.Key;
  static keyD: Phaser.Input.Keyboard.Key;
  static keySpace: Phaser.Input.Keyboard.Key;

  static initialize(scene: Phaser.Scene) {
    this.keyW = scene.input.keyboard.addKey('W');
    this.keyA = scene.input.keyboard.addKey('A');
    this.keyS = scene.input.keyboard.addKey('S');
    this.keyD = scene.input.keyboard.addKey('D');
    this.keySpace = scene.input.keyboard.addKey('SPACE');
  }

  public static getKey(): string {
    if (this.keyW.isDown) {
      return 'up';
    } else if (this.keyA.isDown) {
      return 'left';
    } else if (this.keyS.isDown) {
      return 'down';
    } else if (this.keyD.isDown) {
      return 'right';
    } else if (this.keySpace.isDown) {
      return 'fire';
    } else {
      return 'none';
    }
  }
}

export default Input;
