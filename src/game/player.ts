import Entity from '@game/entity';
import Input from '@game/input';

class Player extends Entity {
  constructor(
    scene: Phaser.Scene,
    positionX: number,
    positionY: number,
    texture: string,
    frame?: string | integer
  ) {
    super(scene, positionX, positionY, texture, frame);

    Input.initialize(scene);

    this.scene.add.existing(this);
    this.scene.physics.add.existing(this);
    this.body.setCollideWorldBounds(true);
    this.setPipeline('cpl');
  }

  protected moveDown(): void {
    this.body.setVelocityY(this.speed);
  }

  protected moveUp(): void {
    this.body.setVelocityY(-this.speed);
  }

  protected moveRight(): void {
    this.body.setVelocityX(this.speed);
  }

  protected moveLeft(): void {
    this.body.setVelocityX(-this.speed);
  }

  protected stayAwhileAndListen(): void {
    this.body.setVelocity(0, 0);
  }

  public move(): void {
    const key = Input.getKey();

    if (key === 'up') {
      this.moveUp();
    } else if (key === 'left') {
      this.moveLeft();
    } else if (key === 'down') {
      this.moveDown();
    } else if (key === 'right') {
      this.moveRight();
    } else if (key === 'fire') {
      // TODO: shoot
      this.scene.cameras.main.shake(500);
    } else if (key === 'none') {
      this.stayAwhileAndListen();
    }
  }
}

export default Player;
