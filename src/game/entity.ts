import * as Phaser from 'phaser';

abstract class Entity extends Phaser.GameObjects.Image {
  public body: Phaser.Physics.Arcade.Body;
  private readonly _speed: number;

  protected constructor(
    scene: Phaser.Scene,
    positionX: number,
    positionY: number,
    texture: string,
    frame?: string | integer
  ) {
    super(scene, positionX, positionY, texture, frame);

    this._speed = 400; // default speed value
  }

  get speed() {
    return this._speed;
  }

  protected abstract moveUp(): void;

  protected abstract moveDown(): void;

  protected abstract moveLeft(): void;

  protected abstract moveRight(): void;

  protected abstract stayAwhileAndListen(): void;
}

export default Entity;
