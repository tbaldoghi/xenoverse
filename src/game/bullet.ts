import * as Phaser from 'phaser';

abstract class Bullet extends Phaser.GameObjects.Image {
  public body: Phaser.Physics.Arcade.Body;
}

export default Bullet;
