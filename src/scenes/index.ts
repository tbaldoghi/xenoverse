import MenuScene from '@scenes/menu.scene';
import GameScene from '@scenes/game.scene';

export { MenuScene, GameScene };
