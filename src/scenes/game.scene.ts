import * as Phaser from 'phaser';
import Player from '@game/player';

const config: Phaser.Types.Scenes.SettingsConfig = {
  active: false,
  visible: false,
  key: 'Game'
};

const shaderFragment: string = ` // ugly ಠ_ಠ
  #ifdef GL_ES
  precision mediump float; // mediump; highp
  #endif

  uniform float u_time;
  uniform vec2 u_resolution; // resolution
  uniform sampler2D u_main_sampler;
  varying vec2 outTexCoord;

  void main() {
    vec2 p = (gl_FragCoord.xy * 2.0 - u_resolution) / min(u_resolution.x, u_resolution.y);
    float l = 0.1 / length(p);

    vec4 texel = texture2D(u_main_sampler, outTexCoord);
    texel *= vec4(0.0, 0.0, abs(sin(l * u_time)), 1.0);
    gl_FragColor = texel;
  }
`;

class CustomPipeline extends Phaser.Renderer.WebGL.Pipelines
  .TextureTintPipeline {}

class GameScene extends Phaser.Scene {
  private shader: Phaser.GameObjects.Shader;
  // private neonShader: Phaser.GameObjects.Shader;
  private player: Player;
  private pipeline: any;
  public shaderTime: number;

  constructor() {
    super(config);

    this.shaderTime = 0;
  }

  public preload(): void {
    this.load.image('player', 'assets/images/player.png');
    this.load.image('plasma', 'assets/images/plasma.png');

    // this.load.glsl('neonShader', 'assets/shaders/neon.frag');
    this.load.glsl('demo', 'assets/shaders/demo.frag');
    this.load.glsl('demo2', 'assets/shaders/demo2.frag');

    const plConfig = {
      game: this.game,
      renderer: this.game.renderer,
      fragShader: shaderFragment
    };

    if (
      !(this.game.renderer instanceof Phaser.Renderer.Canvas.CanvasRenderer)
    ) {
      this.pipeline = this.game.renderer.addPipeline(
        'cpl',
        new CustomPipeline(plConfig)
      );
      this.pipeline.setFloat2(
        'u_resolution',
        this.game.config.width,
        this.game.config.height
      );
    }
  }

  public create(): void {
    this.shader = this.add.shader('demo2', 400, 300, 800, 600);
    this.shader.setInteractive();
    this.player = new Player(this, 400, 550, 'player');
  }

  public update(): void {
    this.pipeline.setFloat1('u_time', this.shaderTime);
    this.shaderTime += 0.1;

    this.player.move();
  }
}

export default GameScene;
