import * as Phaser from 'phaser';

const config: Phaser.Types.Scenes.SettingsConfig = {
  active: false,
  visible: false,
  key: 'Game'
};

class MenuScene extends Phaser.Scene {
  constructor() {
    super(config);
  }

  public create(): void {}

  public update(): void {}
}

export default MenuScene;
