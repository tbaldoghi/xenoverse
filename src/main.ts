import * as Phaser from 'phaser';
import GameScene from '@scenes/game.scene';

const config: Phaser.Types.Core.GameConfig = {
  title: 'xenoverse',
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: 'arcade',
    arcade: {
      debug: true
    }
  },
  render: {
    pixelArt: true
  },
  parent: 'game',
  backgroundColor: '#000000',
  scene: GameScene
};

export const game = new Phaser.Game(config);
