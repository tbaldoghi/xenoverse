#ifdef GL_ES
precision mediump float; // lowp; mediump; highp
#endif

uniform float u_time;
uniform sampler2D u_main_sampler;
varying vec2 outTexCoord; // <-- wtf is this

void main() {
    vec4 texel = texture2D(u_main_sampler, outTexCoord);
    texel *= vec4(0.0, 0.0, abs(sin(u_time)), 1.0);
    
    gl_FragColor = texel;
}
