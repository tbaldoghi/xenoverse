// original: http://glslsandbox.com/e#58378.5

// global remix - Del 30/10/2019
// Total Eclipse :)
// Amiga rulez :) Not realy original eclipse on game but ...
// Gigatron
#ifdef GL_ES
precision highp float;
#endif

#extension GL_OES_standard_derivatives : enable

uniform float time;
uniform vec2 resolution;

float snow(vec2 uv,float scale)
{
	uv *= 2.3;
	float t = time*.25;
	 uv.x+=t/scale-uv.y;
	uv*=scale*.58+sin(uv.y*1.1)*0.05;

	vec2 f= uv+t+( sin(uv.x)*sin(uv.y*1.24));

	float k=1.1,d;

	k=smoothstep(0.,k,sin(f.x+f.y)*0.009);

	return k+k;
}

void main(void){
	vec2 uv=(gl_FragCoord.xy*2.-resolution.xy)/min(resolution.x,resolution.y);
	float dd = 1.0-length( uv*1.2);
	float dd2 =  length(uv*0.35);

	float k1 = sin(dot(uv * 0.75, uv) * 16.0 - time * 6.0);
	float k2 = sin(dot(uv * 0.5, uv) * 8.0 - time * 1.5);

	uv.y *= dot(uv,uv*.11);
	uv.x += sin(time*0.05+dd);
	uv.y += sin(uv.x*.14)*0.221;
	uv.x *= 1.47;
	float c=smoothstep(0.1,0.0,clamp(uv.x*.01+.99,0.,.99));
	c+=snow(uv,3.)*.8;
	c+=snow(uv,5.)*.7;
	c+=snow(uv,7.)*.6;

	c+=snow(uv,9.)*.5;

	c+=snow(uv,11.)*.4;

	c+=snow(uv,13.)*.3;

	c*=(-.99)/dd*dd2;

	c*=1.86*(-.925)/dd+k1;
	vec3 finalColor=(vec3(0.6,0.4,0.76))*c*50.0;
	finalColor *= 7.71+k2;
	dd2=smoothstep(-0.3,1.55,dd2);
	finalColor = mix(finalColor,vec3(1.16+sin(uv.y*0.7+time*1.7),1.1,1.4),sin(uv.y*uv.x)*0.4+dd2*0.67);

	gl_FragColor = vec4(finalColor,1);
}
